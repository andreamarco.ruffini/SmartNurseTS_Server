var express = require('express');
var router = express.Router();

const ServiceType = require('../queries/queries_servicetype');
let servicetype = new ServiceType();
/* GET users listing. */
router.get('/', servicetype.getServiceTypes);

module.exports = router;
