var express = require('express');
var router = express.Router();

const Service =  require('../queries/queries_service');
let service = new Service();
/* GET users listing. */
router.get('/', service.getServices);
router.get('/:appointment', service.getServicesByAppointment);

router.post('/', service.addService);
router.post('/cancel', service.addAnnulationService);

router.put('/:id', service.setDuration);

router.delete('/', service.removeServicesFromAppointment);

module.exports = router;
