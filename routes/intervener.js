var express = require('express');
var router = express.Router();

const Intervener = require('../queries/queries_intervener');
let intervener = new Intervener();
/* GET users listing. */
router.get('/', intervener.getInterveners);
router.get('/:email', intervener.getIntervener);
router.post('/', intervener.login);


module.exports = router;
