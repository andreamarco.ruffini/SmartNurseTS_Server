var express = require('express');
var router = express.Router();

const Address = require('../queries/queries_address');
let address = new Address();
/* GET users listing. */
router.get('/', address.getAddressesByPerson);
router.get('/:id', address.getAddress);

module.exports = router;
