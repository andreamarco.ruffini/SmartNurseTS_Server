var express = require('express');
var router = express.Router();

const Appointment = require('../queries/queries_appointment');
let appointment = new Appointment();

router.get('/', appointment.getAppointments);
router.get('/:id', appointment.getAppointment);

router.post('/', appointment.createAppointment);
router.post('/now', appointment.createAppointmentNow);

router.put('/:id', appointment.startEndAppointment);

router.delete('/:id', appointment.deleteAppointment);

module.exports = router;
