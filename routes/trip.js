var express = require('express');
var router = express.Router();

const Trip = require('../queries/queries_trip');
let trip = new Trip();
/* GET users listing. */
router.get('/', trip.getTrips);
router.get('/:id', trip.getTrip);

router.post('/', trip.createTrip);

module.exports = router;
