var express = require('express');
var router = express.Router();

const Patient = require('../queries/queries_patient');
let patient = new Patient();
/* GET users listing. */
router.get('/', patient.getPatients);
router.get('/:insuranceNbr', patient.getPatient);

module.exports = router;
