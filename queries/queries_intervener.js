var express = require('express');
var Request = require('tedious').Request;
var TYPES = require('tedious').TYPES;
var router = express.Router();
var connection = require('../db');

class Intervener{
    getInterveners(req,res,next){

        var sql = "select * from v_intervener for json path";
        var request = new Request(sql, function(err, rowCount) {
            if (err) {
                next(err);
            }
        });
        
        var jsonArray = "";
        request.on('row', function(columns) {
            columns.forEach(function(column) {
                if (column.value !== null) {
                    jsonArray += column.value;
                }
            });
        });
        
        request.on('doneInProc', function(rowCount, more, rows) {
            if(rowCount == 0){
                jsonArray = "[]";
            }
            res.status(200).json(JSON.parse(jsonArray));
        });
        
        connection.execSql(request);
    }

    getIntervener(req,res,next){
        var email = req.params.email;

        var sql = "select * from v_intervener where email = @email for json path";
        var request = new Request(sql, function(err, rowCount) {
            if (err) {
                next(err);
            }
        });
        request.addParameter('email', TYPES.VarChar, email);

        var jsonArray = "";
        request.on('row', function(columns) {
            columns.forEach(function(column) {
            if (column.value !== null)
                jsonArray += column.value;
            });
        });
        
        request.on('doneInProc', function(rowCount, more, rows) {
            if(rowCount == 0){
                jsonArray = "[]";
            }
            res.status(200).json(JSON.parse(jsonArray));
        });
        
        connection.execSql(request);
    }

    
    login(req,res,next){
        var email = req.body.username;
        var password = req.body.password;

        var sql = "SELECT * FROM V_Intervener WHERE (username = @email OR email = @email) AND password = @password FOR json path";
        var request = new Request(sql, function(err, rowCount) {
            if (err) {
                next(err);
            }
        });
        request.addParameter('email', TYPES.VarChar, email);
        request.addParameter('password', TYPES.VarChar, password);

        var jsonArray = "";
        request.on('row', function(columns) {
            columns.forEach(function(column) {
            if (column.value !== null)
                jsonArray += column.value;
            });
        });
        
        request.on('doneInProc', function(rowCount, more, rows) {
            if(rowCount == 0){
                jsonArray = "[]";
            }
            res.status(200).json(JSON.parse(jsonArray));
        });
        
        connection.execSql(request);
    }
}

module.exports = Intervener;