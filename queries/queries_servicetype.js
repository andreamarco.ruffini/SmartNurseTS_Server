var express = require('express');
var Request = require('tedious').Request;
var TYPES = require('tedious').TYPES;
var router = express.Router();
var connection = require('../db');

class ServiceType{
    getServiceTypes(req,res,next){
        var search = req.query.search;
        var selectable = req.query.selectable;

        if( typeof search !== 'undefined' && search ) {
            search += "%";
            if( typeof selectable !== 'undefined' && selectable ) {
                var sql = 'select * from v_servicetype where isSelectable=@selectable and path LIKE @search for json path';
            } else {
                var sql = 'select * from v_servicetype where path LIKE @search for json path';
            }
        } else {
            if( typeof selectable !== 'undefined' && selectable ) {
                var sql = 'select * from v_servicetype where isSelectable=@selectable for json path';
            } else {
                var sql = 'select * from v_servicetype for json path';
            }
        }
        
        var request = new Request(sql, function(err, rowCount) {
            if (err) {
                next(err);
            }
        });
        request.addParameter('search', TYPES.VarChar, search);
        request.addParameter('selectable', TYPES.VarChar, selectable);
        
        var jsonArray = "";
        request.on('row', function(columns) {
            columns.forEach(function(column) {
                if (column.value !== null) {
                    jsonArray += column.value;
                }
            });
        });
        
        request.on('doneInProc', function(rowCount, more, rows) {
            if(rowCount == 0){
                jsonArray = "[]";
            }
            res.status(200).json(JSON.parse(jsonArray));
        });
        
        connection.execSql(request);
    }
}

module.exports = ServiceType;