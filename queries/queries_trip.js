var express = require('express');
var Request = require('tedious').Request;
var TYPES = require('tedious').TYPES;
var router = express.Router();
var connection = require('../db');

class Trip{
    getTrips(req,res,next){
        var start = req.query.start;
        var destination = req.query.destination;

        var startDefined = typeof start !== 'undefined' && start;
        var destinationDefined = typeof destination !== 'undefined' && destination;

        if(startDefined && destinationDefined){
            var sql = "select * from trip where fk_startingAddress=@start and fk_destinationAddress=@destination for json path";
        }else if(startDefined){
            var sql = "select * from trip where fk_startingAddress=@start for json path";
        }else if(destinationDefined){
            var sql = "select * from trip where fk_destinationAddress=@destination for json path";
        }else{
            var sql = "select * from trip for json path";
        }

        var request = new Request(sql, function(err, rowCount) {
            if (err) {
                next(err);
            }
        });
        request.addParameter('start', TYPES.Int, start);
        request.addParameter('destination', TYPES.Int, destination);
        
        var jsonArray = "";
        request.on('row', function(columns) {
            columns.forEach(function(column) {
                if (column.value !== null) {
                    jsonArray += column.value;
                }
            });
        });
        
        request.on('doneInProc', function(rowCount, more, rows) {
            if(rowCount == 0){
                jsonArray = "[]";
            }
            res.status(200).json(JSON.parse(jsonArray));
        });
        
        connection.execSql(request);
    }

    getTrip(req,res,next){
        var id = req.params.id;

        var sql = "select * from trip where idTrip=@id for json path";
        var request = new Request(sql, function(err, rowCount) {
            if (err) {
                next(err);
            }
        });
        request.addParameter('id', TYPES.Int, id);

        var jsonArray = "";
        request.on('row', function(columns) {
            columns.forEach(function(column) {
            if (column.value !== null)
                jsonArray += column.value;
            });
        });
        
        request.on('doneInProc', function(rowCount, more, rows) {
            if(rowCount == 0){
                jsonArray = "[]";
            }
            res.status(200).json(JSON.parse(jsonArray));
        });
        
        connection.execSql(request);
    }

    createTrip(req,res,next){
        var start = req.body.start;
        var destination = req.body.destination;
        var distance = req.body.distance;
        var duration = req.body.duration;

        var sql = "INSERT INTO Trip(fk_startingAddress,fk_destinationAddress,distance,duration) OUTPUT INSERTED.idTrip VALUES (@start,@destination,@distance,@duration)";
        var request = new Request(sql, function(err, rowCount) {
            if (err) {
                next(err);
            }
        });
        request.addParameter('start', TYPES.Int, start);
        request.addParameter('destination', TYPES.Int, destination);
        request.addParameter('distance', TYPES.Float, distance);
        request.addParameter('duration', TYPES.VarChar, duration);
        
        var jsonArray = "";
        request.on('row', function(columns) {
            columns.forEach(function(column) {
            if (column.value !== null)
                jsonArray += column.value;
            });
        });


        request.on('doneInProc', function(rowCount, more, rows) {
            if(rowCount == 0){
                jsonArray = "[]";
            }
            res.status(200).json(JSON.parse(jsonArray));
        });
        
        connection.execSql(request);
    }
}

module.exports = Trip;