var express = require('express');
var Request = require('tedious').Request;
var TYPES = require('tedious').TYPES;
var router = express.Router();
var connection = require('../db');

class Service{
    getServices(req,res,next){
        var patient = req.query.patient;

        if( typeof patient !== 'undefined' && patient ) {
            var sql = "SELECT V_ServiceType.* FROM V_ServiceType INNER JOIN Service ON fk_service_serviceType=name WHERE fk_service_appointment IN " +
            "(SELECT TOP 1 idAppointment FROM V_Appointment WHERE beginApp<CURRENT_TIMESTAMP AND fk_appointment_patient=@patient ORDER BY beginApp DESC) for json path";
        } else {
            var sql = "select * from v_service for json path";
        }

        var request = new Request(sql, function(err, rowCount) {
            if (err) {
                next(err);
            }
        });
        request.addParameter('patient', TYPES.VarChar, patient);
        
        var jsonArray = "";
        request.on('row', function(columns) {
            columns.forEach(function(column) {
                if (column.value !== null) {
                    jsonArray += column.value;
                }
            });
        });
        
        request.on('doneInProc', function(rowCount, more, rows) {
            if(rowCount == 0){
                jsonArray = "[]";
            }
            res.status(200).json(JSON.parse(jsonArray));
        });
        
        connection.execSql(request);
    }

    getServicesByAppointment(req,res,next){
        var appointment = req.params.appointment;
        
        var sql = 'select * from v_service where fk_service_appointment=@appointment for json path';
        var request = new Request(sql, function(err, rowCount) {
            if (err) {
                next(err);
            }
        });
        request.addParameter('appointment', TYPES.VarChar, appointment);

        var jsonArray = "";
        request.on('row', function(columns) {
            columns.forEach(function(column) {
            if (column.value !== null)
                jsonArray += column.value;
            });
        });
        
        request.on('doneInProc', function(rowCount, more, rows) {
            if(rowCount == 0){
                jsonArray = "[]";
            }
            res.status(200).json(JSON.parse(jsonArray));
        });

        connection.execSql(request);
    }
    
    addService(req,res,next){
        var appointment = req.body.appointment;
        var serviceType = req.body.serviceType;
        var duration = req.body.duration;

        if( typeof duration === 'undefined'){
            duration = '00:00:00';
        }

        //var sql = "INSERT INTO Service(isFinished, duration, fk_service_appointment, fk_service_servicetype) OUTPUT INSERTED.idService SELECT 'true',@duration,@appointment,@serviceType FROM Service WHERE NOT EXISTS (SELECT 1 FROM Service WHERE fk_service_appointment=@appointment AND fk_service_servicetype=@serviceType)";
        var sql = "INSERT INTO Service(isFinished, duration, fk_service_appointment, fk_service_servicetype) OUTPUT INSERTED.idService VALUES('true',@duration,@appointment,@serviceType)";
        var request = new Request(sql, function(err, rowCount) {
            if (err) {
                next(err);
            }
        });
        request.addParameter('appointment', TYPES.Int, appointment);
        request.addParameter('serviceType', TYPES.VarChar, serviceType);
        request.addParameter('duration', TYPES.VarChar, duration);
        
        var jsonArray = "";
        request.on('row', function(columns) {
            columns.forEach(function(column) {
            if (column.value !== null)
                jsonArray += column.value;
            });
        });


        request.on('doneInProc', function(rowCount, more, rows) {
            if(rowCount == 0){
                jsonArray = "[]";
            }
            res.status(200).json(JSON.parse(jsonArray));
        });
        
        connection.execSql(request);
    }
    
    addAnnulationService(req,res,next){
        var appointment = req.body.appointment;

        var sql = "INSERT INTO Service(isFinished, fk_service_appointment, fk_service_servicetype) OUTPUT INSERTED.idService VALUES ('true',@appointment,'Annulation tardive')";
        var request = new Request(sql, function(err, rowCount) {
            if (err) {
                next(err);
            }
        });
        request.addParameter('appointment', TYPES.Int, appointment);
        
        var jsonArray = "";
        request.on('row', function(columns) {
            columns.forEach(function(column) {
            if (column.value !== null)
                jsonArray += column.value;
            });
        });


        request.on('doneInProc', function(rowCount, more, rows) {
            if(rowCount == 0){
                jsonArray = "[]";
            }
            res.status(200).json(JSON.parse(jsonArray));
        });
        
        connection.execSql(request);
    }
    
    setDuration(req,res,next){
        var id = req.params.id;
        var duration = req.body.duration;

        var sql = "UPDATE Service SET duration=@duration, isFinished='true' OUTPUT INSERTED.idService WHERE idService=@id";
        var request = new Request(sql, function(err, rowCount) {
            if (err) {
                next(err);
            }
        });
        request.addParameter('id', TYPES.Int, id);
        request.addParameter('duration', TYPES.VarChar, duration);
        
        var jsonArray = "";
        request.on('row', function(columns) {
            columns.forEach(function(column) {
            if (column.value !== null)
                jsonArray += column.value;
            });
        });


        request.on('doneInProc', function(rowCount, more, rows) {
            if(rowCount == 0){
                jsonArray = "[]";
            }
            res.status(200).json(JSON.parse(jsonArray));
        });
        
        connection.execSql(request);
    }

    removeServicesFromAppointment(req,res,next){
        var id = req.query.id;

        var sql = "DELETE FROM Service OUTPUT DELETED.idService WHERE fk_service_appointment=@id";
        var request = new Request(sql, function(err, rowCount) {
            if (err) {
                next(err);
            }
        });
        request.addParameter('id', TYPES.Int, id);
        
        var jsonArray = "";
        request.on('row', function(columns) {
            columns.forEach(function(column) {
            if (column.value !== null)
                jsonArray += column.value;
            });
        });


        request.on('doneInProc', function(rowCount, more, rows) {
            if(rowCount == 0){
                jsonArray = "[]";
            }
            res.status(200).json(JSON.parse(jsonArray));
        });
        
        connection.execSql(request);
    }
}

module.exports = Service;