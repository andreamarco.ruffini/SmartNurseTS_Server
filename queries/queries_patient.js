var express = require('express');
var Request = require('tedious').Request;
var TYPES = require('tedious').TYPES;
var router = express.Router();
var connection = require('../db');

class Patient{
    getPatients(req,res,next){
        var address = req.query.address;

        if( typeof address !== 'undefined' && address ) {
            var sql = "SELECT v_patient.* FROM v_patient INNER JOIN Address_Person ON idPerson=fk_person WHERE fk_address=@address for json path";
        } else {
            var sql = "SELECT * FROM v_patient for json path";
        }

        var request = new Request(sql, function(err, rowCount) {
            if (err) {
                next(err);
            }
        });
        request.addParameter('address', TYPES.Int, address);
        
        var jsonArray = "";
        request.on('row', function(columns) {
            columns.forEach(function(column) {
                if (column.value !== null) {
                    jsonArray += column.value;
                }
            });
        });
        
        request.on('doneInProc', function(rowCount, more, rows) {
            if(rowCount == 0){
                jsonArray = "[]";
            }
            res.status(200).json(JSON.parse(jsonArray));
        });
        
        connection.execSql(request);
    }

    getPatient(req,res,next){
        var insuranceNbr = req.params.insuranceNbr;

        var sql = "select * from v_patient where insuranceNbr = @insuranceNbr for json path";
        var request = new Request(sql, function(err, rowCount) {
            if (err) {
                next(err);
            }
        });
        request.addParameter('insuranceNbr', TYPES.VarChar, insuranceNbr);

        var jsonArray = "";
        request.on('row', function(columns) {
            columns.forEach(function(column) {
            if (column.value !== null)
                jsonArray += column.value;
            });
        });
        
        request.on('doneInProc', function(rowCount, more, rows) {
            if(rowCount == 0){
                jsonArray = "[]";
            }
            res.status(200).json(JSON.parse(jsonArray));
        });
        
        connection.execSql(request);
    }

    /*getPatients(req,res,next){
        var request = new Request("select * from v_patient for json path", function(err, rowCount) {
            if (err) {
            console.log(err);
            } else {
            console.log(rowCount + ' rows');
            }
            //connection.close();
        });
        
        var jsonArray = "";
        request.on('row', function(columns) {
            columns.forEach(function(column) {
            if (column.value === null) {
                console.log('NULL');
            } else {
                console.log(column.value);
                jsonArray += column.value;
            }
            });
        });
        
        request.on('doneInProc', function(rowCount, more, rows) {
            res.status(200).json(JSON.parse(jsonArray));
        });
        
        connection.execSql(request);
    }*/
}

module.exports = Patient;