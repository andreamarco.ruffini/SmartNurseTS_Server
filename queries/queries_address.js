var express = require('express');
var Request = require('tedious').Request;
var TYPES = require('tedious').TYPES;
var router = express.Router();
var connection = require('../db');

class Address{
    getAddressesByPerson(req,res,next){
        var person = req.query.person;

        if( typeof person !== 'undefined' && person ) {
            var sql = "SELECT Address.*, Address_Person.isDefault FROM Address INNER JOIN Address_Person ON idAddress=fk_address WHERE fk_person=@person for json path";
        } else {
            var sql = "SELECT * FROM Address for json path";
        }

        var request = new Request(sql, function(err, rowCount) {
            if (err) {
                next(err);
            }
        });
        request.addParameter('person', TYPES.Int, person);
        
        var jsonArray = "";
        request.on('row', function(columns) {
            columns.forEach(function(column) {
                if (column.value !== null) {
                    jsonArray += column.value;
                }
            });
        });
        
        request.on('doneInProc', function(rowCount, more, rows) {
            if(rowCount == 0){
                jsonArray = "[]";
            }
            res.status(200).json(JSON.parse(jsonArray));
        });
        
        connection.execSql(request);
    }

    getAddress(req,res,next){
        var id = req.params.id;

        var sql = "SELECT * FROM Address WHERE idAddress=@id for json path";
        var request = new Request(sql, function(err, rowCount) {
            if (err) {
                next(err);
            }
        });
        request.addParameter('id', TYPES.Int, id);

        var jsonArray = "";
        request.on('row', function(columns) {
            columns.forEach(function(column) {
            if (column.value !== null)
                jsonArray += column.value;
            });
        });
        
        request.on('doneInProc', function(rowCount, more, rows) {
            if(rowCount == 0){
                jsonArray = "[]";
            }
            res.status(200).json(JSON.parse(jsonArray));
        });
        
        connection.execSql(request);
    }
}

module.exports = Address;