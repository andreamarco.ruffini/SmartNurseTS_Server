var express = require('express');
var Request = require('tedious').Request;
var TYPES = require('tedious').TYPES;
var router = express.Router();
var connection = require('../db');

class Appointment{
    getAppointments(req,res,next){
        var intervener = req.query.intervener;
        var nextDays = req.query.nextDays;

        if( typeof intervener !== 'undefined' && intervener ) {
            if( typeof nextDays !== 'undefined' && nextDays ) {
                var sql = "select * from v_appointment where fk_appointment_intervener = @intervener AND scheduledBegin BETWEEN CURRENT_TIMESTAMP AND DATEADD(day,@days,CURRENT_TIMESTAMP) ORDER BY scheduledBegin ASC for json path";
            } else {
                var sql = "select * from v_appointment where fk_appointment_intervener = @intervener AND scheduledBegin>CURRENT_TIMESTAMP ORDER BY scheduledBegin ASC for json path";
            }
        }else if( typeof nextDays !== 'undefined' && nextDays ) {
                var sql = "select * from v_appointment where scheduledBegin BETWEEN CURRENT_TIMESTAMP AND DATEADD(day,@days,CURRENT_TIMESTAMP) ORDER BY scheduledBegin ASC for json path";
        } else {
                var sql = "select * from v_appointment ORDER BY idAppointment ASC for json path";
        }

        var request = new Request(sql, function(err, rowCount) {
            if (err) {
                next(err);
            }
        });
        request.addParameter('intervener', TYPES.VarChar, intervener);
        request.addParameter('days', TYPES.Int, nextDays);
        
        var jsonArray = "";
        request.on('row', function(columns) {
            columns.forEach(function(column) {
                if (column.value !== null) {
                    jsonArray += column.value;
                }
            });
        });
        
        request.on('doneInProc', function(rowCount, more, rows) {
            if(rowCount == 0){
                jsonArray = "[]";
            }
            res.status(200).json(JSON.parse(jsonArray));
        });
        
        connection.execSql(request);
    }

    getAppointment(req,res,next){
        var id = req.params.id;

        var sql = "select * from v_appointment where idAppointment=@id for json path";
        var request = new Request(sql, function(err, rowCount) {
            if (err) {
                next(err);
            }
        });

        var jsonArray = "";
        request.on('row', function(columns) {
            columns.forEach(function(column) {
            if (column.value !== null)
                jsonArray += column.value;
            });
        });
        request.addParameter('id', TYPES.Int, id);
        
        request.on('doneInProc', function(rowCount, more, rows) {
            if(rowCount == 0){
                jsonArray = "[]";
            }
            res.status(200).json(JSON.parse(jsonArray));
        });
        
        connection.execSql(request);
    }

    createAppointment(req,res,next){
        var scheduledBegin = req.body.scheduledBegin;
        var scheduledEnd = req.body.scheduledEnd;
        var intervener = req.body.intervener;
        var patient = req.body.patient;

        var sql = "INSERT INTO Appointment(scheduledBegin,scheduledEnd,fk_appointment_intervener,fk_appointment_patient) OUTPUT INSERTED.idAppointment VALUES (@scheduledBegin,@scheduledEnd,@intervener,@patient)";
        var request = new Request(sql, function(err, rowCount) {
            if (err) {
                next(err);
            }
        });
        request.addParameter('scheduledBegin', TYPES.VarChar, scheduledBegin);
        request.addParameter('scheduledEnd', TYPES.VarChar, scheduledEnd);
        request.addParameter('intervener', TYPES.VarChar, intervener);
        request.addParameter('patient', TYPES.VarChar, patient);
        
        var jsonArray = "";
        request.on('row', function(columns) {
            columns.forEach(function(column) {
            if (column.value !== null)
                jsonArray += column.value;
            });
        });


        request.on('doneInProc', function(rowCount, more, rows) {
            if(rowCount == 0){
                jsonArray = "[]";
            }
            res.status(200).json(JSON.parse(jsonArray));
        });
        
        connection.execSql(request);
    }

    createAppointmentNow(req,res,next){
        var intervener = req.body.intervener;
        var trip = req.body.trip;
        var patient = req.body.patient;

        var sql = "INSERT INTO Appointment(beginApp,fk_appointment_intervener,fk_appointment_trip,fk_appointment_patient) OUTPUT INSERTED.idAppointment VALUES (CURRENT_TIMESTAMP,@intervener,@trip,@patient)";
        var request = new Request(sql, function(err, rowCount) {
            if (err) {
                next(err);
            }
        });
        request.addParameter('intervener', TYPES.VarChar, intervener);
        request.addParameter('trip', TYPES.Int, trip);
        request.addParameter('patient', TYPES.VarChar, patient);
        
        var jsonArray = "";
        request.on('row', function(columns) {
            columns.forEach(function(column) {
            if (column.value !== null)
                jsonArray += column.value;
            });
        });

        request.on('doneInProc', function(rowCount, more, rows) {
            if(rowCount == 0){
                jsonArray = "[]";
            }
            res.status(200).json(JSON.parse(jsonArray));
        });
        
        connection.execSql(request);
    }

    startEndAppointment(req,res,next){
        var action = req.query.action;
        var id = req.params.id;

        if( typeof action !== 'undefined' && action ) {
            switch(action){
                case 'start':
                    var sql = "UPDATE Appointment SET beginApp=CURRENT_TIMESTAMP WHERE idAppointment=@id";
                    break;
                case 'end':
                    var sql = "UPDATE Appointment SET endApp=CURRENT_TIMESTAMP WHERE idAppointment=@id";
                    break;
            }
        }
        var request = new Request(sql, function(err, rowCount) {
            if (err) {
                next(err);
            }
        });
        request.addParameter('id', TYPES.Int, id);

        request.on('doneInProc', function(rowCount, more, rows) {
            res.status(200).json(JSON.parse(rowCount));
        });
        
        connection.execSql(request);
    }

    deleteAppointment(req,res,next){
        var id = req.params.id;

        var sql = "DELETE FROM Appointment WHERE idAppointment=@id";
        var request = new Request(sql, function(err, rowCount) {
            if (err) {
                next(err);
            }
        });
        request.addParameter('id', TYPES.Int, id);

        request.on('doneInProc', function(rowCount, more, rows) {
            res.status(200).json(JSON.parse(rowCount));
        });
        
        connection.execSql(request);
    }
}

module.exports = Appointment;