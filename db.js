var Connection = require('tedious').Connection;  

var config = require('./config/settings.json');

var connection = new Connection(config); 

connection.on('connect', function(err) { 
    if(!err){
      console.log("Connected");
    }else{
      console.log("Problem during connection!");
    }
});

module.exports = connection;